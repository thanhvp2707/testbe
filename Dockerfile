FROM python:3.8

WORKDIR /app
ADD . .
RUN pip install -r requirement.txt

EXPOSE 5000

CMD ["python", "main.py"]
